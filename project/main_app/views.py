from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from .models import Client, Mailing, Tag, Operator
from .serializers import (
    ClientSerializer, MailingSerializer, TagSerializer,
    OperatorSerializer, MailingStatSerializer, DetailMailingStatSerializer
)


class TagViewSet(ModelViewSet):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()


class OperatorViewSet(ModelViewSet):
    serializer_class = OperatorSerializer
    queryset = Operator.objects.all()


class ClientViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, GenericViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MailingViewSet(ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    @action(detail=False)
    def general_stat(self, request):
        serializer = MailingStatSerializer(Mailing.get_stat())
        return Response(serializer.data)

    @action(detail=True)
    def stat(self, request, pk=None):
        obj = self.get_object()
        serializer = DetailMailingStatSerializer(obj.get_detail_stat())
        return Response(serializer.data)
