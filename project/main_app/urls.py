from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, MailingViewSet, TagViewSet

app_name = 'main_app'

router = DefaultRouter()
router.register('client', ClientViewSet, basename='client')
router.register('mailing', MailingViewSet, basename='mailing')
router.register('tag', TagViewSet, basename='tag')

urlpatterns = [
    *router.urls
]
