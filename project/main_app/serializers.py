from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from timezone_field.rest_framework import TimeZoneSerializerField

from .models import Client, Tag, Mailing, Operator


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')


class OperatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operator
        fields = ('id', 'name')


class ClientSerializer(serializers.ModelSerializer):
    phone = PhoneNumberField()
    timezone = TimeZoneSerializerField()

    class Meta:
        model = Client
        fields = ('id', 'phone', 'timezone', 'tag', 'operator')


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'start', 'end', 'text', 'tag', 'operator')


class MailingStatSerializer(serializers.Serializer):
    message_count = serializers.IntegerField()
    count = serializers.IntegerField()
    waiting = serializers.IntegerField()
    send = serializers.IntegerField()
    error = serializers.IntegerField()
    overdue = serializers.IntegerField()


class DetailMailingStatSerializer(serializers.Serializer):
    message_count = serializers.IntegerField()
    waiting = serializers.IntegerField()
    send = serializers.IntegerField()
    error = serializers.IntegerField()
    overdue = serializers.IntegerField()
